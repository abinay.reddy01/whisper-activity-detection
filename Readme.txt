Main file train.py
Sample test file is provided to understand weights extraction.

Technical details are provided in the paper

https://spire.ee.iisc.ac.in/spire/papers_pdf/Abhinay_Interspeech_2020.pdf

Naini, Abinay Reddy, Malla Satyapriya, and Prasanta Kumar Ghosh. "Whisper Activity Detection Using CNN-LSTM Based Attention Pooling Network Trained for a Speaker Identification Task." INTERSPEECH. 2020.
