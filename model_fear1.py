import tensorflow as tf
#import tensorflow_probability as tfp
import numpy as np
from keras.layers import Input
from keras.layers import Dense, Dropout, Activation
from keras.layers import LSTM
#from TDNNLayer import TDNNLayer
from keras.layers import Input
from keras.layers import Conv1D, Conv2D,MaxPooling1D, AveragePooling1D, Concatenate,Activation, GlobalAveragePooling1D, GlobalAveragePooling2D, AveragePooling2D, Flatten, GlobalMaxPooling1D, Lambda, LSTM, TimeDistributed, BatchNormalization, Add, Reshape,UpSampling1D, SeparableConv1D
#import h5py
import time
from keras.models import Model
from keras.utils import Sequence
import keras.callbacks
import keras.backend as K
from keras.initializers import Identity, Constant,RandomNormal, Zeros
import os
import math
import scipy.io

def GetModel(filters=40, kernel_size=(3,4)):
       	F=24;
        #ivec = Input(batch_shape=(1,100));
       	inp=Input(batch_shape=(1,None,F,1));
        #wts1 = AveragePooling2D(pool_size=(4, 1), strides= None, padding='valid')(inp)
        inp1 = Reshape((-1,24))(inp)
        #inp2 = AveragePooling1D(pool_size= 4, strides= None, padding='valid')(inp1)
        #wts = TimeDistributed(Dense(1,activation='sigmoid'))(inp1)
        wts1 = LSTM(24,return_sequences=True)(inp1)
        wts = LSTM(1,return_sequences=True)(wts1)
        wts_out = Activation('sigmoid')(wts)
        inp222 = AveragePooling1D(pool_size= 4, strides= None, padding='valid')(wts_out)
        #inp2 = tf.expand_dims(inp222, axis=3)   #.shape.as_list()
        inp2 = Lambda(tile)(inp222)
        wt1 = LSTM(24,return_sequences=True)(inp1)
        wt = LSTM(24,return_sequences=True)(wt1)
        wt_out = Activation('sigmoid')(wt)
        inp3333 = GlobalAveragePooling1D()(wt_out)
        #inp333 = tf.expand_dims(inp3333, axis=1)    #.shape.as_list()
        #inp3 = tf.expand_dims(inp333, axis=3)      #.shape.as_list()
        inp3 = Lambda(tile1)(inp3333)
        cnn_1=Conv2D(filters, kernel_size, padding='same', activation='relu')(inp)
        cnn_1_n=BatchNormalization()(cnn_1)
        cnn11_n = AveragePooling2D(pool_size=(2, 1), strides= None, padding='valid')(cnn_1_n)
        cnn_2=Conv2D(filters, kernel_size, strides=1, padding='same', activation='relu')(cnn11_n)
        cnn_2_n=BatchNormalization()(cnn_2)
        cnn_22_n = AveragePooling2D(pool_size=(1, 1), strides=None, padding='valid')(cnn_2_n)                                                                                  
        cnn_3=Conv2D(20, kernel_size, strides=1, padding='same', activation='relu')(cnn_22_n)
        cnn_3_n=BatchNormalization()(cnn_3)
        cnn_4 = AveragePooling2D(pool_size=(2, 1), strides=None, padding='valid')(cnn_3_n)
        #cnn_5=Conv2D(30, kernel_size, strides=1, padding='same', activation='relu')(cnn_4)
        #cnn_5_n=BatchNormalization()(cnn_5)
        #cnn_6=Conv2D(30, kernel_size, strides=1, padding='same', activation='relu')(cnn_5_n)
        #cnn_6_n=BatchNormalization()(cnn_6)
        #cnn   = Reshape((-1,240))(cnn_4)
        dnn = Lambda(mult)([cnn_4,inp2])
        dnn1 = Lambda(mult)([dnn,inp3])
        #lstm_4= CuDNNLSTM(60, return_sequences=True)(dnn)
                                
        #lstm_5= CuDNNLSTM(80,return_sequences=True)(cnn)                   
        #illum_est = Lambda(norm)(lstm_5);
        layer_5_n = GlobalAveragePooling2D()(dnn1)
        #layer_2= Dense(100,activation='linear')(ivec)
        
        layer_5= Dense(120,activation='relu')(layer_5_n)
        #layer_con = Concatenate()([layer_5,layer_2])
        
        #layer_5_n=BatchNormalization()(layer_5)
        #layer_5_1 = Dense(300,activation='relu')(layer_5_n)
        #tf.keras.layers.
        layer_6= Dense(5,activation='softmax')(layer_5)

        return(inp,layer_6)
           
        
         
           
def norm(fc2):
    cnn = K.squeeze(fc2, axis = 3)
    #tf.convert_to_tensor(arg, dtype=tf.float32)
    return cnn
              
def mult(d1):
    #k = tf.ones(tf.shape(d1[1]))
    mu = tf.math.multiply(d1[0],d1[1])   #tf.math.subtract(k,d1[1]))
    #tf.convert_to_tensor(arg, dtype=tf.float32)
    return mu

def tile(d2):
    #k = tf.constant([1,1,24,40], tf.int32)
    mu1 = tf.expand_dims(d2, axis=3)  #tf.math.subtract(k,d1[1]))
    #tf.convert_to_tensor(arg, dtype=tf.float32)
    return mu1

def tile1(d3):
    k1 = tf.expand_dims(d3, axis=1)
    mu2 = tf.expand_dims(k1, axis=3)   #tf.math.subtract(k,d1[1]))
    #tf.convert_to_tensor(arg, dtype=tf.float32)
    return mu2




        
    
    
